

$(function(){
    // $('.content').hide();
    $('#spinner').delay(1000000).hide();
    // $('.content').delay(50000).show();

    $('.accordions').accordion(
    );
    $('#darkTheme').on('click', function() {
        $('body').css({
            'background-color':"#222f3e",
        });
        $('.accordion').css({
            background:"#2F4F4F",
            hover:"#8FDADF",
        });
        $('.panel').css({
            background:"#D3D3D3",
        });
        $('.content').css({
            color:'white',
        });
        $('button').css({
            color:'white'
        });
        $('.panel').css({
            color: 'black',
        });
        $('#landing').css({
            color: 'black'
        });

    });

    $('#blueTheme').on('click', function() {
        $('body').css({
            background:"#00FFFF",
        });
        $('.accordion').css({
            background:"#40E0D0",
        });
        $('.panel').css({
            background:"#AFEEEE",
        });
        $('.content').css({
            color:'black',
        });
        $('button').css({
            color:'black'
        });
    });

});
